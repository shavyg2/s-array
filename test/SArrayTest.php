<?php


use Shavy\Collection\SArray;
class SArrayTest extends PHPUnit_Framework_TestCase{


    /**
     * @return SArray
     */
    public function array_provider(){
        $array=new SArray;
        $array->add("banana","bread");
        return $array;
    }



    public function test_is_iterable(){
        $array=$this->array_provider();
        echo "\r";
        foreach($array as $a){
            $this->assertTrue($a!==null);
        }
    }

    public function test_same_class(){
        $array=$this->array_provider();
        $this->assertTrue($array->is_same_class(new SArray));
    }

    public function test_merge(){
        $array1=$this->array_provider();
        $array2=$this->array_provider();
        $array1->merge($array2);
        $this->assertEquals(4,$array1->length());
        $this->assertEquals(array("banana","bread","banana","bread"),$array1->toArray());

    }

    public function test_insert(){
        /**
         * @var SArray $array
         */
        $array= SArray::_add(1,3);
        $array->insert(2,1);
        $this->assertEquals(array(1,2,3),$array->toArray());
    }

    public function test_each(){
        $array=$this->array_provider();

        $array->each(function($data){
            return "I love ".$data;
        });

        $this->assertEquals(array("I love banana","I love bread"),$array->toArray());
    }

    public function test_invoke(){
        $array=$this->array_provider();
        $array->kv("pizza",array("topping"=>"cheese"));
        $this->assertEquals('cheese',$array("pizza.topping"));

        $array->kv("pizza.size",10);
        $this->assertEquals(10,$array('pizza.size'));

    }


    public function test_file_cache(){
        $array= new SArray();
        $array->set_text("my text","look here");
        $this->assertEquals("look here",$array->get_text("my text"));
    }
}
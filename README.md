SArray
======

PHP Extended Array
-------------------


###Examples
> You can use it to add item s to an array list like so.

    $array=new SArray;//create object
    
    $array->add("banana","bread"); //add to the array. Think of it like ['banana','bread']
    
    
    
> You can use it to get to the objects using a forloop. 
    
    foreach($array as $a){
            echo $a->get($i)
        }    


> If you want to get the index of an item you can use the get method and tyoe it in.

    $array->get($i);
    

>If you like the dot syntax it is already there for you and you can use it as such. To set a value for something you can do this.

    $array->kv("person.firstname","bill");//neat eh?
    $array->kv("person.lastname","bob");
    $array->kv("person.age",5);
    
>This will create an array that will look like this:

    array(
        "person"=>array(
            "firstname"=>"bill",
            "lastname"=>'bob',
            "age"=>5
        )
    );
    
    
>You can get the people back by using dot-syntax like this as well

    $array('person.firstname');//returns bill
    $array("person.age");//returns 5
    
    //This isn't a typo, you don't need a method name. This will key it very nice and short. 
    
    
    
>What about if you want to store something like a file name with no dot syntax


    $array("filename.ext"); 
    // this will try to get array['filename']['ext']
    
    //want you really want is 
    
    $array("filename.ext",false)
    
>What about if you want to overwrite or add a new array or SArray.

    //you can use
    $array->overwrite(SArray $data) //only takes in a super array
    $array->array_overwrite($data) //regular array
    // You can tell it exactly what to do. There is no checking in these methods
    


> If that is a little too complicated you can just use the replace method

    $array->replace($data);
    //Let the program figure it out. 
    //it will check types
    
> You have you standard toArray method. 
This will go all the way down and convert objects to array and other arrays as well.

> You can also use insert to insert into the middle of an array

    $array->insert($data) //you can insert an array for a multi dimension array
    $array->insertBatch($data)// you can insert and array of items
    /*
    
    
    $array=SArray::_add("item1","item4");//you can use _ to create the object and add something to it right away
    $array->insert(array("item2","item3"),1);
    
    // This will make 
    /*
    
        array(
            "item1",
            array(
                "item2",
                "item3"
            ),
            "item4"
        )
        but what you really want is 
    */


    $array=SArray::_add("item1","item4");//you can use _ to create the object and add something to it right away
    $array->insertBatch(array("item2","item3"),1);
    /*
    
        array(
            "item1",
            "item2",
            "item3"
            "item4"
        )
        more like it
    */
    
    
> To get part of an array you can use sub($start,$end). This will return a plain array

    $array->sub($start,$end);
    $array->sub_as_SArray($start,$end);//they are different methods will likely add shorter version in update
    
    $array->length();// this is not calculated so safe to use in for loops
    
    $array->dump();// var_dump for you  
    
    $array->pretty_print();// print_r not it is not the same as print_r($array)
    
    // pretty_print converts to array then prints
    
    //same as
    
    print_r($array->toArray());








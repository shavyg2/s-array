<?php

namespace Shavy\Collection;
use Shavy\Collection\Contracts\SArrayInterface;
use Shavy\QuickCache\QCache;

@include __DIR__ . "/../../vendor/autoload.php";


class SArray implements SArrayInterface, \Iterator
{



    private $_id;
    private static $global_id;

    const NOT_AN_ARRAY = "Error: Class is not An Array or SArray";
    const UNKNOWN_METHOD = "Error: Method is Unknown";
    const INDEX_OUT_OF_BOUND = "Error: The Index is out of bounds";

    private $array;
    private $position = 0;
    private $length;

    /**
     * @var QCache $sarray
     */
    private $sarray;

    public function __construct()
    {
        $this->array = array();
        return $this;
    }


    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->array[$this->position];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return array_keys($this->array)[$this->position];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->array[$this->position]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->position = 0;
    }


    public function add()
    {
        $arg = func_get_args();
        foreach ($arg as $a)
            $this->push($a);
        return $this;
    }

    public function kv($key, $value,$dot_sync=true)
    {
        if(is_string($key) and $dot_sync){
            $array=&$this->array;
            $keys=explode(".",$key);
            foreach($keys as $key){
                if(!isset($array[$key])){
                    $array[$key]=array();
                    $array=&$array[$key];
                }else{
                    if($this->is_same_class($array[$key])){
                        $array=&$array[$key]->array;
                    }elseif(is_array($array[$key])){
                        $array=&$array[$key];
                    }else{
                        $array[$key]=array();
                        $array=&$array[$key];
                    }
                }
            }
            $array=$value;
        }else{
            $this->array[$key] = $value;
        }

        return $this;
    }

    public function re_index()
    {
        $this->array = array_values($this->array);
        $this->update_length();
        return $this;
    }

    public function filter(callable $function)
    {
        for ($i = 0; $i < $this->length(); $i++) {
            $this->array[$i] = call_user_func($function, $this->array[$i]);
        }
        $this->array = array_filter($this->array);
        $this->update_length();
        return $this;
    }


    public function contains_key($key)
    {
        return array_key_exists($key, $this->all());
    }

    public function push($item)
    {
        $this->array[] = $item;
        $this->length++;
        return $this;
    }

    /**
     * @param $array
     * @return SArray $this
     * @throws \Exeception
     */
    public function merge($array)
    {
        if ($this->is_same_class($array)) {
            $this->merge_with_self($array);
        } elseif (is_array($array)) {
            for ($i = 0, $size = count($array); $i < $size; $i++)
                $this->push($array[$i]);
        } else {
            throw new \Exeception(SArray::NOT_AN_ARRAY);
        }
        return $this;
    }

    public function is_same_class($array)
    {
        return (is_object($array) && \get_class($array) === 'Shavy\Collection\SArray');
    }

    private function merge_with_self(SArray $array)
    {
        $this->merge($array->array);
        return $this;
    }


    public function replace($array)
    {
        if ($this->is_same_class($array)) {
            $this->overwrite($array);
        } elseif (is_array($array)) {
            $this->array_overwrite($array);
        } else {
            throw new \Exception(self::NOT_AN_ARRAY);
        }
        return $this;
    }

    public function overwrite(SArray $array)
    {
        $this->length = $array->length;
        $this->position = $array->position;
        $this->array = $array->array;
        return $this;
    }

    public function array_overwrite($array)
    {
        $this->position = 0;
        $this->array = $array;
        $this->length = count($array);
        return $this;
    }

    private function update_length()
    {
        $this->length = count($this->array);
        return $this;
    }

    public function toArray()
    {
        return self::_to_array_bundle($this->all());
    }

    private static function to_array($object)
    {
        if (self::_is_same_class($object)) {
            /**
             * @var SArray $object
             */
            return $object->toArray();
        } elseif (is_array($object)) {
            return self::_to_array_bundle($object);
        } elseif (is_object($object)) {
            return self::_to_array_bundle(get_object_vars($object));
        } else {
            return $object;
        }
    }

    private static function to_array_bundle($objects)
    {
        if (is_array($objects)) {
            foreach($objects as &$object){
                $object = self::to_array($object);
            }
        }
        unset($object);
        return $objects;
    }


    public function insert($item, $position = null)
    {
        if ($position === null) {
            $this->push($item);
        } else {
            $this->insertBatch(array($item), $position);
        }
        return $this;
    }

    public function get($index)
    {
        if(isset($this->array[$index])){
           $answer=$this->array[$index];
        }else{
            $answer=$this->get_text($index);
        }
        return $answer;
    }

    public function get_as_SArray($index)
    {

    }

    public function sub($start, $end = null)
    {
        if ($end === null) {
            $end = $this->length();
        }
        $smaller = array();
        for ($i = $start; $i < $end; $i++) {
            $smaller[] = $this->get($i);
        }

        return $smaller;
    }

    public function sub_as_SArray($start, $end = null)
    {
        $array = $this->sub($start, $end);
        $s_array = self::getInstance();
        return $s_array->replace($array);
    }


    public function insertBatch($array, $position)
    {

        if ($position >= $this->length()) {
            return $this->merge($array);
        }

        $front = $this->sub_as_SArray(0, $position);
        $middle = $array;
        $end = $this->sub($position);
        return $this->replace($front->merge($middle)->merge($end));
    }

    public function length()
    {
        return $this->size();
    }

    public function size()
    {
        return $this->length;
    }


    public function dump()
    {
        var_dump($this->all());
    }


    public static function getInstance()
    {
        return new SArray;
    }

    /**
     * @param $method
     * @param $args
     * @return SArray
     */
    public static function __callStatic($method, $args)
    {
        if ($method[0] === '_')
            $method = substr($method, 1);
        else
            throw new \Exception(self::UNKNOWN_METHOD);
        return self::not_static($method, $args);
    }


    private static function not_static($method, $args)
    {
        $array = self::getInstance();
        return call_user_func_array(array($array, $method), $args);
    }


    public function pretty_print()
    {
        print_r($this->toArray());
    }

    public function each(callable $function)
    {
        if (is_callable($function)) {
            for ($i = 0; $i < $this->length(); $i++) {
                $this->array[$i] = call_user_func($function, $this->array[$i]);
            }
        } else
            return false;
    }


    public function __invoke($string,$dot_sync=true)
    {
        if (is_string($string) and $dot_sync) {
            $search = explode(".", $string);
            $item = $this->array;
            foreach ($search as $key) {
                $item = $item[$key];
            }
            return $item;
        }

        return $this->array[$string];
    }


    public function each_key_value(callable $function){

        $keys=array_keys($this->array);
        for ($i = 0; $i < $this->length(); $i++) {
            $this->array[$i] = call_user_func($function,$keys[$i], $this->array[$i]);
        }
    }


    public function set_text($key,$value){
        if($this->sarray===null){
            $this->sarray=new QCache();
            $this->sarray->set_text($key,$value);
        }
    }

    public function get_text($key){
        return $this->sarray->get_text($key);
    }

    public function all(){

        $one=$this->array;
        $two=isset($this->sarray)?$this->sarray->all():array();
        return array_merge($one,$two);
    }

}
